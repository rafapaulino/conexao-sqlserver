﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace ConexaoSQLserverOleDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OleDbConnection conexao = new OleDbConnection(@"Provider=SQLOLEDB;Driver={SQL Server};Server=M002158\MSSQLSERVER01;Database=banco_aula;Uid=rafa;Pwd=123456;");
            OleDbCommand sql = new OleDbCommand("select * from bairro", conexao);
            try
            {
                conexao.Open();
                OleDbDataReader lendoDados = sql.ExecuteReader();
                while(lendoDados.Read())
                {
                    richTextBox1.Text += lendoDados["bai_codigo"] + " - " + lendoDados["bai_nome"] + "\r\n";
                }
                lendoDados.Close();
                conexao.Close();

            }
            catch(OleDbException erro)
            {
                MessageBox.Show("Erro = " + erro);
            }
        }
    }
}
